-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 18-Ago-2017 às 19:58
-- Versão do servidor: 10.1.25-MariaDB
-- PHP Version: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sebrae`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos`
--

CREATE TABLE `cursos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `start` datetime NOT NULL,
  `finish` datetime NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cursos`
--

INSERT INTO `cursos` (`id`, `title`, `category`, `description`, `price`, `start`, `finish`, `avatar`, `name`, `address`, `city`) VALUES
(2, 'Consultoria Presencial de Marketing', 'Mercado e vendas', 'Se você possui um pequeno negócio e deseja melhorar o desempenho da sua gestão, planejar estratégias para vencer a atual crise econômica e está em busca de um trabalho personalizado, conheça a Consultoria Empresarial do Sebrae.', '150.00', '2017-08-18 10:24:20', '2017-08-19 12:28:24', 'https://randomuser.me/api/portraits/women/86.jpg', 'Melinda Perez', 'Av. Barão Homem de Melo 329\r\nNova Granada', 'Belo Horizonte'),
(4, 'Desenvolvimento de jogos ', 'Games ', 'Se você possui um pequeno negócio e deseja melhorar o desempenho da sua gestão, planejar estratégias para vencer a atual crise econômica e está em busca de um trabalho personalizado, conheça a Consultoria Empresarial do Sebrae.', '950.00', '2017-08-18 10:24:20', '2017-08-19 12:28:24', 'https://randomuser.me/api/portraits/women/86.jpg', 'Paulo Birro', 'Rua cambé 310 ap 101 Coqueiros', 'Belo Horizonte'),
(5, 'Teste BackEnd Sebrae', 'Mercado e vendas', 'onno non nono no onnonono no nono no no no nonono no no no no on no on onno no no no no nono no no no no no noon on no no no no no on onon on on no on on onon on on no no on on on on onno no on no', '750.00', '2017-08-18 10:24:20', '2017-08-19 12:28:24', 'https://randomuser.me/api/portraits/women/86.jpg', 'Mario World', 'Av. Barão Homem de Melo 329\r\nNova Granada', 'Contagem'),
(6, 'Consultoria e planejamento', 'Mercado e vendas', 'Se você possui um pequeno negócio e deseja melhorar o desempenho da sua gestão, planejar estratégias para vencer a atual crise econômica e está em busca de um trabalho personalizado, conheça a Consultoria Empresarial do Sebrae.', '150.00', '2017-08-18 10:24:20', '2017-08-19 12:28:24', 'https://randomuser.me/api/portraits/women/86.jpg', 'Melinda Perez', 'Av. Barão Homem de Melo 329\r\nNova Granada', 'Belo Horizonte'),
(7, 'Curso repetido de teste', 'Games ', 'Se você possui um pequeno negócio e deseja melhorar o desempenho da sua gestão, planejar estratégias para vencer a atual crise econômica e está em busca de um trabalho personalizado, conheça a Consultoria Empresarial do Sebrae.', '950.00', '2017-08-18 10:24:20', '2017-08-19 12:28:24', 'https://randomuser.me/api/portraits/women/86.jpg', 'Paulo Birro', 'Rua cambé 310 ap 101 Coqueiros', 'Belo Horizonte'),
(8, 'Outro curso clonado apenas de teste', 'Mercado e vendas', 'Se você possui um pequeno negócio e deseja melhorar o desempenho da sua gestão, planejar estratégias para vencer a atual crise econômica e está em busca de um trabalho personalizado, conheça a Consultoria Empresarial do Sebrae.', '150.00', '2017-08-18 10:24:20', '2017-08-19 12:28:24', 'https://randomuser.me/api/portraits/women/86.jpg', 'Melinda Perez', 'Av. Barão Homem de Melo 329\r\nNova Granada', 'Belo Horizonte');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
