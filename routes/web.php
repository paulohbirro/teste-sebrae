<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::group(['middleware' => ['web']], function(){
    Route::resource('cursos', 'CursosController');
});

Route::group(['middleware' => ['web']], function(){
    Route::resource('/', 'CursosController');
});




Route::get('show/{id}', 'CursosController@show');

Route::post('/buscar', 'CursosController@buscar');
Route::post('show/buscar', 'CursosController@buscar');






