<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Date\Date;
use Illuminate\Support\Facades\DB;      



use App\Cursos;

class CursosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       


     
       	//Date::setLocale('pt-BR');


       $Cursos = Cursos::all();

		
       return view('cursos.index',['todosCursos' => $Cursos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }



public function buscar(Request $request)
{
    //Recupera o userName do input
    $usuario = $request->input('txt_busca');


     $Cursos = Cursos::where('title', $usuario)
    ->orWhere('title', 'like', '%' .$usuario. '%')->get();

       
       //return view('cursos.index',['todosCursos' => $Cursos]);
         return view('dados',['todosCursos' => $Cursos]);
}


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


            $Cursos = Cursos::find($id);
            

        

         return view('detalhes',compact('Cursos','dados'));

      

          }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
