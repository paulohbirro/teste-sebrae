<!DOCTYPE html>
<html> 
<head>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<title>Detalhes</title>


<style type="text/css">

body{background-color: #C9C9C9;}
#detalhes{width: auto; height: 100px;}
.demo-card-event.mdl-card {	max-width: 700px; width: auto; height: 475px; background: #ffffff; margin-top:-48px;}
.demo-card-event > .mdl-card__actions {border-color: rgba(255, 255, 255, 0.2);}
.demo-card-event > .mdl-card__title {align-items: flex-start;}
.demo-card-event > .mdl-card__title > h4 { margin-top: 0;}
.demo-card-event > .mdl-card__actions {display: flex;box-sizing:border-box;align-items: center;}
.demo-card-event > .mdl-card__actions > .material-icons { padding-right: 10px;}
.demo-card-event > .mdl-card__title,
.demo-card-event > .mdl-card__actions,
.demo-card-event > .mdl-card__actions > .mdl-button {color: black;}
.material-icons-event{ color:#C9C9C9; }
.txt_menor{font-size:14px;margin-bottom:-10px; }
.mdl-grid.center-items { justify-content: center;}

.alinha_texto{	padding-left: 10px;	color: #757575;	font-size: 14px;}
.texto-sub{color: #757575;font-size: 13px;}
.titulo{font-size:14px;color: black;}

</style>
</head>
<body>

<div class=" mdl-layout--fixed-drawer mdl-layout--fixed-header">
<header class="mdl-layout__header" style="background-color: #01A9F4;">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
	<div class="mdl-layout__header-row">

	<span class="mdl-layout-title">
			<a href="javascript:history.back()">
		      		<i style="color: white;" class="material-icons">keyboard_backspace</i>
		      </a>
	</span>

	</div>
</header>



	  <!-- Body -->

	<div class="mdl-grid center-items">
	   

	<div class="content-grid mdl-grid content-column mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-cell--top" id="detalhes">

	<div class="demo-card-event mdl-card mdl-shadow--2dp ">

	  <div class="mdl-cell mdl-cell--12-col">
	   
	  <span class="titulo"> {{  $Cursos->title }}</span>
	   
	  </div>


	  <div class="mdl-cell mdl-cell--12-col" >
	   
	  <span class="texto-sub"> {{  $Cursos->description }} </span>

	   


	  </div>

	  <div class="mdl-cell mdl-cell--12-col">
	   
	 <i  class="material-icons material-icons-event">event</i>

	  <span class="alinha_texto">  {{ $Cursos->start->format(' j F Y ')}}  </span>


	  </div>


	  <div class="mdl-cell mdl-cell--12-col">
	   
	 <i class="material-icons material-icons-event">&#xE8B5;</i>

	  

	  <span class="alinha_texto">   De {{ $Cursos->start->format(' h:i:s')}} as  {{ $Cursos->finish->format(' h:i:s')}}  - 
	</span>

	  </div>



	  <div class="mdl-cell mdl-cell--12-col">
	   
	 <i   class="material-icons material-icons-event">room</i>

	  <span class="alinha_texto">  {{ $Cursos->address}} </span>



	  </div>


	 


	    <div class="mdl-cell mdl-cell--12-col">
	   
	 <i   class="material-icons material-icons-event">attach_money</i>

	 <span class="alinha_texto">  R$ {{ $Cursos->price}}  </span>


	  </div>


	   <div class="mdl-cell mdl-cell--12-col">
	   
	 <i   class="material-icons material-icons-event">label</i>

	 <span class="alinha_texto">  {{ $Cursos->category}}   </span>


	  </div>

	  

	   <div class="mdl-cell mdl-cell--12-col">
	   

	<div>
	<img src="{{ $Cursos->avatar}}" style="border-radius: 50%; width: 40px; ">
	<span class="alinha_texto">{{ $Cursos->name}}</span>
	<div>


	     


	  </div>




	</div>

	</div>
	 
	    <div class="mdl-cell mdl-cell--12-col" style="">
	    	
	    	<center>	<button class="mdl-button mdl-button--raised mdl-button--accent"  style="background:#FF9101; color:black;">INSCRIÇÃO</button></center>

	    </div>

	    </div>
	</div>

	  </body>
	  </html>




