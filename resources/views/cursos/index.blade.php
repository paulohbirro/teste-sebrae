   <!DOCTYPE html>
   <html>
   <head>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <title>teste</title>

  <!-- The drawer is always open in large screens. The header is always shown,
  even in small screens. -->

  <style type="text/css">
  /* TODO: Hover and Focus state.
 *       Fix position of mobile search button.
 */
 .mdh-expandable-search {
  margin: 0 50px;
  align-items: center;
  justify-content: center;
}

.mdh-expandable-search form {
  max-width: 600px;
}

.mdh-expandable-search,
.mdh-expandable-search form,
.mdh-expandable-search input {
  /* Cross browser flex-grow */
  -webkit-box-flex: 1;
  -webkit-flex-grow: 1;
  -ms-flex-positive: 1;
  flex-grow: 1;
}

.mdh-expandable-search,
.mdh-expandable-search form {
  /* Cross browser inline-flex */
  display: -webkit-inline-box;
  display: -webkit-inline-flex;
  display: -moz-inline-box;
  display: -ms-inline-flexbox;
  display: inline-flex;
}

/* Position search icon */
.mdh-expandable-search .material-icons {
  position: relative;
  right: -40px;
  margin-left: -24px; /* Remove the blank space left behind by the icon being relatively positioned */
}

.mdh-expandable-search input {
  outline: none;
  border: none;
  font-size: 16px;
  color: #FFFFFF;
  background-color: #54C4F7;
  padding: 0px 35px 0px 70px;
  height: 40px;
  line-height: 40px; /* TODO: This was recommended for cross browser compatability of input height, check if its actually needed in modern browsers */
  
  border-radius: 5px 5px 5px 5px;
  -moz-border-radius: 5px 5px 5px 5px;
  -webkit-border-radius: 5px 5px 5px 5px;
}

.mdh-expandable-search input::-webkit-input-placeholder { /* WebKit browsers */
  color:    #FFFFFF;
}
.mdh-expandable-search input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
 color:    #FFFFFF;
 opacity:  1; /* Firefox gives the placeholder a reduced opacity so we have to increase it */
}
.mdh-expandable-search input::-moz-placeholder { /* Mozilla Firefox 19+ */
 color:    #FFFFFF;
 opacity:  1; /* Firefox gives the placeholder a reduced opacity so we have to increase it */
}
.mdh-expandable-search input:-ms-input-placeholder { /* Internet Explorer 10+ */
 color:    #FFFFFF;
}


/* Bug fix: https://github.com/google/material-design-lite/issues/1078
* To much padding on the left of header when the menu button is hidden */
@media screen and (min-width: 851px) {
  .mdl-layout__header-row {
    padding: 0 40px 0 40px;
  }
}

/* Bug fix for badges being in the wrong location */
.mdl-badge[data-badge]:after {
  right: -7px;
  top: -8px;
}

body{

  background-color: #C9C9C9;
}

.demo-card-event.mdl-card {
  width: 290px;
  height: 100px;
  background: #ffffff;
  margin-top: 10px;
}
.demo-card-event > .mdl-card__actions {
  border-color: rgba(255, 255, 255, 0.2);
}
.demo-card-event > .mdl-card__title {
  align-items: flex-start;
}
.demo-card-event > .mdl-card__title > h4 {
  margin-top: 0;
}
.demo-card-event > .mdl-card__actions {
  display: flex;
  box-sizing:border-box;
  align-items: center;
}
.demo-card-event > .mdl-card__actions > .material-icons {
  padding-right: 10px;
}
.demo-card-event > .mdl-card__title,
.demo-card-event > .mdl-card__actions,
.demo-card-event > .mdl-card__actions > .mdl-button {
  color: black;
}
.content-grid {
  max-width: 960px;
}

.material-icons-event{ color:#C9C9C9; }

.txt_menor{font-size:14px;margin-bottom:-10px; }
.link{text-decoration: none; color: black;}

</style>

<script type="text/javascript">

// TODO: this got hacky quick..... needs major refactor
//       hide the two icons if the search box is below a certain size.
$(document).ready(function() {
  $('.mdh-toggle-search').click(function() {
    // No search bar is currently shown
    if ($(this).find('i').text() == 'search') {
      $(this).find('i').text('cancel');
      $(this).removeClass('mdl-cell--hide-tablet mdl-cell--hide-desktop'); // Ensures the close button doesn't disappear if the screen is resized.

      $('.mdl-layout__drawer-button, .mdl-layout-title, .mdl-badge, .mdl-layout-spacer').hide();
      $('.mdl-layout__header-row').css('padding-left', '16px'); // Remove margin that used to hold the menu button
      $('.mdh-expandable-search').removeClass('mdl-cell--hide-phone').css('margin', '0 16px 0 0');
      
    }
    // Search bar is currently showing
    else {
      $(this).find('i').text('search');
      $(this).addClass('mdl-cell--hide-tablet mdl-cell--hide-desktop');
      
      $('.mdl-layout__drawer-button, .mdl-layout-title, .mdl-badge, .mdl-layout-spacer').show();
      $('.mdl-layout__header-row').css('padding-left', '');
      $('.mdh-expandable-search').addClass('mdl-cell--hide-phone').css('margin', '0 50px');
    }
    
  });
});

</script>


</head>
<body>

  <div class=" mdl-layout--fixed-drawer
  mdl-layout--fixed-header">

  <!-- Header -->
  <header class="mdl-layout__header" style="background-color: #01A9F4;">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <div class="mdl-layout__header-row">

      <span class="mdl-layout-title">Cursos</span>

      <!-- Displayed on Computer and Tablet -->
      <!-- Search -->
      <div class="mdh-expandable-search mdl-cell--hide-phone">
        <i class="material-icons">search</i>
        <form action="buscar" method="post">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="text" name="txt_busca" placeholder="Pesquisar" size="1">
        </form>
      </div>

      <!-- Displayed on mobile -->
      <div class="mdl-layout-spacer mdl-cell--hide-tablet mdl-cell--hide-desktop"></div>
      <!-- Search button -->
      <button class="mdh-toggle-search mdl-button mdl-js-button mdl-button--icon mdl-cell--hide-tablet mdl-cell--hide-desktop">
        <i class="material-icons">search</i>
      </button>

      <!-- Buttons -->
    </div>
  </header>

  <!-- Body -->

  <div class="content-grid mdl-grid">

    @foreach ($todosCursos as $Cursos)

    <div  class="content-column mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-cell--top">

      <div class="demo-card-event mdl-card mdl-shadow--2dp">

        <div class="mdl-card__title mdl-card--expand" style="font-size: 12px;color: #7A7A7A;">

         {{  $Cursos->category }}

       </div>


       <div class="mdl-card__title mdl-card--expand" style="margin-top:-45px; font-size: 13px;" >

         <a class="link" href="show/{{  $Cursos->id }}"> {{  $Cursos->title }} </a>

       </div>

       <div class="mdl-card__title txt_menor" style="margin-top:-50px;color:#7A7A7A;">

         {{  $Cursos->city }}

       </div>
       <hr>
       <div class="mdl-card__title mdl-card--expand txt_menor" style="margin-top:-10px;color:#7A7A7A;">
         {{ $Cursos->start->format(' j F Y ')}}        
         <div class="mdl-layout-spacer">
         </div>
         <i class="material-icons material-icons-event">event</i>
       </div>
     </div>
   </div>
   @endforeach
 </div>  
</body>
</html>











